FROM openjdk:8-jdk-alpine

RUN apk update
RUN apk add --no-cache gradle

RUN rm -rf /var/cache/apk

